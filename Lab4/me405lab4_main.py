"""
@file   me405lab4_main.py
@brief  This file perpectually collects and stores temperature data into array until keyboard interrupt is triggered.
@detail This file creates an MCP9808 object and uses it to first check if the device is connected, then take temperature
        readings every minute. Time is tracked using utime.sleep_ms().  This separates temp collection by approximately 60 seconds.
        Each temperature reading and time stamp is stored within its own array until a keyboard interrupt occurs, at which time,
        the buffers are written to a .CSV file which can be pulled from the nucleo using the "get" command in a terminal window
        and then plotted in excel.
@author Michelle Chandler, Hunter Brooks
@date   February 2,2021
"""

import pyb
from pyb import I2C
from mcp9808 import mcp9808
import utime
import array

## @brief   This array stores temperature data measured from the microcontroller
## @details This data in this array is then written to a .csv file once ctrl+c is pressed
amb_temp = array.array('f', [])

## @brief   This array stores temperature data measured from the MCP9808 temp sensor
## @details This data in this array is then written to a .csv file once ctrl+c is pressed
mcu_temp = array.array('f', [])

## @brief   This array stores timestamp corresponding to each measured pair of temp data
## @details This data in this array is then written to a .csv file once ctrl+c is pressed
time     = array.array('f', [])

## @brief   Master i2c object
## @details Create object on bus one as master
print('i2c object')
i2c = I2C(1, I2C.MASTER)

## @brief   Adcall object
## @details Create object with 12 bit resolution on internal channels
print('adcall object')
adcall = pyb.ADCAll(12, 0x70000)

## @brief   MCP object for MCP 9808 Driver
## @details Initializes the mcp9808 driver and imports the i2c object for the i2c interface
mcp9808 = mcp9808(i2c)

## @brief   Variable containing the temperate reading number/time value depending on interval
## @details This variable is appended to the time array each time data is collected
time_stamp= 0

## @brief   Call check function which scans for i2c devices on bus and returns address, checking it against the input address
## @details If the correct address is detected, "found" is printed, else "not found" is printed
check = mcp9808.check(24)

while True: 
    try:
        adcall.read_vref() # without this command the adcall object returns weird core temps
        
        ## @brief   Variable that stores the internal microcontroller temperature 
        ## @details This value is then appended to the mcu_temp array
        mcu_temp_var = adcall.read_core_temp() # reading the mcu core temperature
        mcu_temp.append(mcu_temp_var) # save mcu temperature to array
        
        ## @brief   Variable that stores the MCP temperature sensor temperature 
        ## @details This value is then appended to the amb_temp array
        amb_temp_var = mcp9808.celsius() # read the ambient temperature
        amb_temp.append(amb_temp_var) # save the ambient temp reading to array
        
        time.append(time_stamp)
        print('Time [min]: '+ str(time_stamp) + '   STM32 Temp [C]: ' + "{:.2f}".format(mcu_temp_var) + '   MCP9808 Temp[C]: ' + "{:.2f}".format(amb_temp_var))
        mcu_temp_var = None  # reset the microcontroller temp value
        amb_temp_var = None  # reset the MCP9808 temp value
        time_stamp = time_stamp + 1  # increment current time in minutes
        utime.sleep_ms(60000) # delay for 60 seconds before next temp collection

    except KeyboardInterrupt: 
        # these lines enough to open and close file
        with open ("me405lab4.csv", "w") as me405lab4:
            # how much will write into file) 
            me405lab4.write ('Time [min]: , Amb_temp: , mcu_temp: \n')
            for n in range(len(mcu_temp)):
                # what to write into file
                me405lab4.write ('{:}, {:}, {:}\n'.format(time[n], amb_temp[n], mcu_temp[n]))
        print ("The file has by now automatically been closed.")
        break 