'''
@file      MotorDriver_TP.py
@brief     This file contains a motor driver that includes all the features associated with driveing a DC motor connected to the DRV8847.
@details   This driver contains seven total methods that perform all of the necessary functions for the motor objects.  These methods include:
           (1): __init__() - initializes motor objects, (2): define_interrupts() - defines external interrupts that handle fault states, (3): 
           enable() - enables the motor objects, (4): disable() - disables the motor objects, (5): set_duty() - sets the duty cycle for the 
           motor objects, (6): nFault() - external interrupt callback function that puts motors in fault state by disabling and locking motors
           until source of fault has been removed when the nFAULT pin (pin B2) goes low, (7): Fault_Cleared() - external interrupt callback
           function that removes motors from fault state by unlocking motors and enabling them once the source of the fault has been removed
           and the user button has been pressed. Setting up PWM is accomplished by initalizing an internal timer object to PWM mode,
           configuring two pins to two of the timer's channels and configuring a third nSLEEP pin that acts as an active-high enable pin. 
           Once this is complete, the direction of the motor rotation is controlled by which timer channel receives a pulse width percentage.
           The motors start rotating once the nSLEEP pin is set high and the rotational speed is controlled by the values used for the pulse
           width percentage.  The motors stop rotating when the nSLEEP pin is set low or if a fault is detected. When a fault is detected,
           the nFAULT pin will go low, triggering an external interrupt which runs a function that disables and locks the motors.
           Once the source of the fault has been removed, the user can press the blue user button to unlock and enable the motors.
@author    Hunter Brooks and Ryan Funchess
@date      March 8, 2021
'''
# Import the necessary module for this driver.
import pyb

# The following two lines of code are used to receive an error message if an error occurs when executing the program.
import micropython
micropython.alloc_emergency_exception_buf(200)  # the buffer contains 200 bytes of memory

class MotorDriver:
    '''
    @brief   This class implements a motor driver for the ME405 board.
    @details This driver contains seven total methods that perform all of the necessary functions for the motor objects.  These methods include:
             (1): __init__() - initializes motor objects, (2): define_interrupts() - defines external interrupts that handle fault states, (3): 
             enable() - enables the motor objects, (4): disable() - disables the motor objects, (5): set_duty() - sets the duty cycle for the 
             motor objects, (6): nFault() - external interrupt callback function that puts motors in fault state by disabling and locking motors
             until source of fault has been removed when the nFAULT pin (pin B2) goes low, (7): Fault_Cleared() - external interrupt callback
             function that removes motors from fault state by unlocking motors and enabling them once the source of the fault has been removed
             and the user button has been pressed. Setting up PWM is accomplished by initalizing an internal timer object to PWM mode,
             configuring two pins to two of the timer's channels and configuring a third nSLEEP pin that acts as an active-high enable pin. 
    '''
    # Class Variable:
    
    ## @brief   this is a fault flag that will lock the motors when a fault is detected
    #  @details this flag is set high through an external interrupt callback function when
    #           the nFAULT pin goes low and the flag is set low through a separate external
    #           interrupt callback function when the fault is removed and the user button is pressed
    
    global nFault_flg     # this is a defined as a global variable so both motors will be locked out during fault state
    nFault_flg = 0        # initially, the fault flag is low
    
    def __init__ (self, pin_nSLEEP, pin_IN1, pin_IN2, timer, motor_number, pin_nFault, pin_Button):
        ''' 
        @brief Creates a motor driver by initializing pins and an internal timer, defines external interrupts, and turnes the motor off for safety.
        @param pin_nSLEEP     A pyb.Pin object to use as the enable pin.
        @param pin_IN1        A pyb.Pin object to use as the input to half bridge 1.
        @param pin_IN2        A pyb.Pin object to use as the input to half bridge 2.
        @param timer          A pyb.Timer object to use for PWM generation on
                              IN1_pin and IN2_pin.
        @param motor_number   An integer representing which motor an object is being created for
        @param pin_nFault     A pyb.Pin object connected to nFault external interrupt.
        @param pin_Button     A pyb.Pin object connected to Fault_Cleared external interrupt.
        '''
        ## @brief   A pyb.Pin object to use as the enable pin.   
        ## @details This is specified when creating a motor object.                             
        self.pin_nSLEEP   = pin_nSLEEP

        ## @brief   A pyb.Pin object to use as the input to half bridge 1.
        ## @details This is specified when creating a motor object.
        self.pin_IN1      = pin_IN1
        
        ## @brief   A pyb.Pin object to use as the input to half bridge 2.
        ## @details This is specified when creating a motor object. 
        self.pin_IN2      = pin_IN2
        
        ## @brief   A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        ## @details This is specified when creating a motor object.
        self.timer        = timer

        ## @brief   An integer representing which motor an object is being created for
        ## @details This is specified so that only one set of nFault interrupts are defined
        self.motor_number = motor_number
        
        ## @brief   A pyb.Pin object connected to nFault external interrupt.
        ## @details This is specified when creating a motor object. 
        self.pin_nFault   = pin_nFault
        
        ## @brief   A pyb.Pin object connected to Fault_Cleared external interrupt.
        ## @details This is specified when creating a motor object. 
        self.pin_Button   = pin_Button
        
        self.pin_nSLEEP.low()  # begin with the motor off for safety
        
        # The following if statement initializes the proper timer channels depending on which motor the motor object is being created for
        if self.motor_number == 1:
            ## @brief   TimerChannel object on Timer 3, CH. 1
            ## @details This object configures the timer in PWM mode (active high) 
            #           and configures the correct pins for this timer channel
            self.t3ch1 = self.timer.channel(1,pyb.Timer.PWM,pin=self.pin_IN1)   # initialize TimerChannel object on TIM. 3 CH. 1,configure the timer in PWM mode (active high), configure pinB4 for this timer channel
            
            ## @brief   TimerChannel object on Timer 3, CH. 2
            ## @details This object configures the timer in PWM mode (active high) 
            #           and configures the correct pins for this timer channel
            self.t3ch2 = self.timer.channel(2,pyb.Timer.PWM,pin=self.pin_IN2)   # initialize TimerChannel object on TIM. 3 CH. 2,configure the timer in PWM mode (active high), configure pinB5 for this timer channel
        else:
            self.t3ch1 = self.timer.channel(3,pyb.Timer.PWM,pin=self.pin_IN1)   # initialize TimerChannel object on TIM. 3 CH. 3,configure the timer in PWM mode (active high), configure pinB0 for this timer channel
            self.t3ch2 = self.timer.channel(4,pyb.Timer.PWM,pin=self.pin_IN2)   # initialize TimerChannel object on TIM. 3 CH. 4,configure the timer in PWM mode (active high), configure pinB1 for this timer channel
       
        # The following if statement only allows for the external interrupts to be defined to motor one. An error would occur if both motors had the external interrupts defined on them.
        # The assumption is that motor one will be created before motor two
        if self.motor_number == 1:
            self.define_interrupts()
        else:
            pass
        print ('Creating a motor driver')
    
    def define_interrupts(self):
        '''
        @brief         Defines the external interrupts on motor one object.
        @details       The nFault and button interrupts are used to deal with fault scenarios.
        '''
        ## @brief   Definition of external interrupt that puts the motors in fault state when triggered
        ## @details While in fault state, the motors are disabled and cannot be enabled.
        self.nFaultInt   = pyb.ExtInt(self.pin_nFault,mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE,callback=self.nFault)
        
        ## @brief   Definition of external interrupt that removes the motors from fault state when triggered.
        ## @details To remove the motors from fault state, the nFault_flg is cleared and the motors are enabled.
        self.ButtonInt   = pyb.ExtInt(self.pin_Button,mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE,callback=self.Fault_Cleared)
       
    def enable (self):
        '''
        @brief         Enables the motor driver if not in fault state.
        @details       This is accomplished by setting the nSLEEP pin high.
        '''
        # The global nFault_flg is referenced in this function to determine if motors are locked or not. If locked, the motors cannot be enabled.
        global nFault_flg    
        if nFault_flg == 0:
            self.pin_nSLEEP.high()                      # enable motor driver by setting nSLEEP pin high  
            print ('Enabling Motor')
        else:
            print('The user must clear fault and then press blue user button before enabling motors.')
        
    def disable (self):
        '''
        @brief         Disables the motor driver.
        @details       This is accomplished by setting the nSLEEP pin low.
        '''
        self.pin_nSLEEP.low()                      # disable motor driver by setting nSLEEP pin low 
        print ('Disabling Motor')
        
    def set_duty (self, duty):
        '''
        @brief   This method sets the duty cycle to be sent to the motor to the given level. 
        @details Positive values cause effort in one direction, negative values in the opposite direction. Acceptable duty cycles
                 are betwen -100 and 100, all others will be rejected.
        @param  duty A signed integer holding the duty cycle of the PWM signal sent to the motor.
        '''
        
        ## @brief   Variable containing desired duty cycle magnitude specified by user
        ## @details This value can span from -100 to 100. Negative values are in reverse and positive rotate 
        #           the motors in the positive direction.
        self.duty = duty
        
        # With the following if statement, the specified duty cycle is compared to the acceptable range of values
        # and if it's acceptable, the duty cycle on the appropriate motor is updated.
        
        #if self.duty <= 100 and self.duty >= -100:
        if self.duty > 0:
            
            # Motor running forward: Set duty cycle on channel 1 to  appropriate magnitude and set duty cycle on channel 2 to zero
            self.t3ch1.pulse_width_percent(self.duty)                 # sets the inital pulse width percentage to duty
            self.t3ch2.pulse_width_percent(0)                         # sets the inital pulse width percentage to 0
            
        elif self.duty < 0:
            
            # Motor running in Reverse: Set duty cycle on channel 2 to appropriate magnitude and set duty cycle on channel 1 to zero
            self.t3ch1.pulse_width_percent(0)                         # sets the inital pulse width percentage to 0
            self.t3ch2.pulse_width_percent(abs(self.duty))            # sets the inital pulse width percentage to absolute value of duty
        
        else:
            
            # Motor stopped: Set duty cycle on both channels to zero
            self.t3ch1.pulse_width_percent(self.duty)                 # sets the inital pulse width percentage to duty
            self.t3ch2.pulse_width_percent(self.duty)                 # sets the inital pulse width percentage to duty
        #else:
          #  print('Invalid input duty cycle.  Acceptable duty cycles are between -100 and 100')
            
    def nFault(self,x):
        '''
        @brief   This method sets the motors to fault state, which disables and locks motors.
        @details This is called when the nFAULT pin goes low. Setting nFault_flg high makes 
                 user unable to enable motors until flag is lowered.
        @param   placeholder variable to satisfy callback function requirement
        '''
        global nFault_flg
        if self.pin_nFault.value() == 0:
            #nFault_flg = 1
            #self.disable()
            print('A fault has been detected. The source of the fault must be eliminated before motors can be enabled.')
            print('Once the source of the fault has been eliminated, press the blue user button to enable the motors.')
        else: 
            pass
            
    def Fault_Cleared(self,x):
        '''
        @brief   This method removes the motors from fault state, which enables and unlocks motors.
        @details This is called when the user button is pressed after the source of the fault is cleared.
                 Setting nFault_flg low allows users to enable motors.
        @param   placeholder variable to satisfy callback function requirement
        '''
        global nFault_flg
        if nFault_flg == 0:
            pass
        else:
            nFault_flg = 0
            self.enable()
            print('The fault has been cleared and the motors have been enabled.')
            
if __name__ =='__main__':
    # The following code represents a test program for the motor class.
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver

    ## @brief   Pin object representing nSLEEP pin
    ## @details This object is attached to pin A15 and is set to output    
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP) 
    
    ## @brief   Pin object representing nFAULT pin
    ## @details This object is attached to pin B2 and set to input      
    pin_nFAULT  = pyb.Pin(pyb.Pin.cpu.B2,pyb.Pin.IN) 

    ## @brief   Pin object representing user button
    ## @details This object is attached to pin C13 and used to enable motor after a fault is triggered      
    pin_Button  = pyb.Pin(pyb.Pin.cpu.C13)                 
    
    ## @brief   Pin object representing the first input pin of motor one
    ## @details This object is attached to pin B4 and set to output  
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4,pyb.Pin.OUT_PP) 
    
    ## @brief   Pin object representing the second input pin of motor one
    ## @details This object is attached to pin B5 and set to output    
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5,pyb.Pin.OUT_PP)   

    ## @brief   Pin object representing the first input pin of motor two
    ## @details This object is attached to pin B0 and set to output      
    pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0,pyb.Pin.OUT_PP)   
    
    ## @brief   Pin object representing the second input pin of motor two
    ## @details This object is attached to pin B1 and set to output 
    pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1,pyb.Pin.OUT_PP)   
    
    # Create the timer object used for PWM generation
    
    ## @brief   Timer object used for PWM generation
    ## @details Uses timer 3 at 20kHz
    timer  = pyb.Timer(3,freq=20000)                       
    
    # Create a motor object passing in the pins and timer

    ## @brief   Motor object for first motor
    ## @details This object contains the nFLEEP, nFault, user button, timer, 
    #           and motor one pin objects. This object also specifies it is for motor one
    moe1     = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, timer, 1, pin_nFAULT, pin_Button)
    
    ## @brief   Motor object for second motor
    ## @details This object contains the nFLEEP, nFault, user button, timer, 
    #           and motor two pin objects. This object also specifies it is for motor two
    moe2     = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, timer, 2, pin_nFAULT, pin_Button)
   
    # Set the duty cycle to 35 percent
    moe1.set_duty(35)
    moe2.set_duty(35)

    # Enable the motor driver
    moe1.enable()
    moe2.enable()