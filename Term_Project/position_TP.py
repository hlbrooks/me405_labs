'''
@file      position_TP.py
@brief     This file contains a hardware driver to interface a resistive touch panel with the STM 32 microcontroller.
@details   The class for this driver is named position and it contains a constructor that takes in eight total inputs, 
           including: four arbitrary pins (xp,xm,yp,and ym), the width (x-dimension) and length (y-dimension) of the
           touch panel, and the x and y distances from the bottom left corner of the touch panel to the center of the 
           touch panel.  The driver contains the following modules: "x_coord" - measures and returns the horizontal 
           position in units of mm of a point of contact from the center of the touch panel; "y_coord" - measures and
           returns the vertical position in units of mm of a point of contact from the center of the touch panel; 
           "z_coord" - measures and returns boolean True if there is contact with the touch panel or False if no 
           contact is sensed; "my_position" - runs "x_coord", "y_coord", and "z_coord" and returns a tuple containing 
           their outputs in that order. The measurement the functions receive are voltage divider ratios from one of 
           two potentiometers (one measuring x direction and one measuring y direction).  This reading is normalized 
           to the specified overall length and width of the touch panel and then offset according to the specified 
           distance to the center of the touch panel. The resistive touch panel this driver was designed to interface 
           with will be used to indicate the planar position of the ball on the balance platform for this course's 
           final project.
@author    Hunter Brooks
@date      March 4, 2021
'''
# The following two lines of code are used to receive an error message if an error occurs when executing the program.
import micropython
micropython.alloc_emergency_exception_buf(200)  # the buffer contains 200 bytes of memory

# Import the necessary modules for this driver
import pyb
from pyb import Pin
from pyb import ADC
import utime

class position:
    '''
    @brief    This class is a driver for the resistive touch panel
    @details  This class allows you to measure the location of a contact point on the 
              resistive touch panel.  To do so, the four arbitrary pins(xm,xp,ym,and yp)
              the dimensions of the touch panel (width and length) and the x and y 
              coordinates of its center must be specified in the constructor
    '''              

    # Class Variables:
        # Calibration ADC Limits - These were used to calibrate the x_coord and y_coord
        # modules. They represent the measuring limits in the x and y directions since the
        # touch panel is not capable of measuring the entire area of the panel (width x length)
    
    ## @brief   this is the minimum readable measurement in the x direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the left edge. This is used in x_coord() to account for the reduced
    #           reading span in the x direction
    x_min = 210
    
    ## @brief   this is the maximum readable measurement in the x direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the right edge. This is used in x_coord() to account for the reduced
    #           reading span in the x direction
    x_max = 3794
    
    ## @brief   this is the minimum readable measurement in the y direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the bottom edge. This is used in y_coord() to account for the reduced
    #           reading span in the y direction
    y_min = 375
    
    ## @brief   this is the maximum readable measurement in the x direction of the touch panel
    #  @details when I ran my finger across the length of the touch panel, this was the value being
    #           output when I reached the top edge. This is used in y_coord() to account for the reduced
    #           reading span in the y direction
    y_max = 3549
    
    def __init__(self,xp,xm,yp,ym,w,l,x_c,y_c):
        '''
        @brief         initializes the "position" touch panel driver and assigns the desired attributes
                       to the object for the touch panel - STM 32 mcu interface
        @param PIN.xp  positive x pin that will be manipulated in the following modules
        @param PIN.xm  minus x pin that will be manipulated in the following modules
        @param PIN.yp  positive y pin that will be manipulated in the following modules
        @param PIN.ym  minus y pin that will be manipulated in the following modules
        @param w       Width of the touch panel as specified in the documenation from manufacturer
        @param l       Length of the touch panel as specified in the documenation from manufacturer
        @param x_c     X-distance from the bottom left corner of the touch panel to its center
        @param y_c     Y-distance from the bottom left corner of the touch panel to its center
        '''
        ## @brief   A pyb.Pin object to use as the x-plus pin.
        ## @details This is specified when creating a position object.
        self.PIN_xp = xp
        
        ## @brief   A pyb.Pin object to use as the x-minus pin.
        ## @details This is specified when creating a position object.        
        self.PIN_xm = xm
        
        ## @brief   A pyb.Pin object to use as the y-plus pin.
        ## @details This is specified when creating a position object.        
        self.PIN_yp = yp
        
        ## @brief   A pyb.Pin object to use as the y-minus pin.
        ## @details This is specified when creating a position object.        
        self.PIN_ym = ym
        
        ## @brief   Width of touch panel in units of mm
        ## @details This value was specified manufacturer documentation         
        self.w      = w
        
        ## @brief Length of touch panel in units of mm
        ## @details This value was specified manufacturer documentation
        self.l      = l
        
        ## @brief   X-distance from bottom left edge of touch panel to center, in units of mm
        ## @details We assume the x-distance to the center of the touch panel is half of the width measurement
        self.x_c    = x_c
        
        ## @brief Y-distance from bottom left edge of touch panel to center, in units of mm
        ## @details We assume the y-distance to the center of the touch panel is half of the length measurement
        self.y_c    = y_c

        ## @brief   Variable containing the x distance of a point of contact from the center of the panel
        ## @details This value is calculated in x_coord()        
        self.my_x = 0
        
        ## @brief   Variable containing the y distance of a point of contact from the center of the panel
        ## @details This value is calculated in y_coord()
        self.my_y = 0

    def x_coord (self):
        '''
        @brief          This method reads the horizontal potentiometer and outputs an x-distance from center for a contact point
        @details        This method initializes pins and sets them to input/output, high/low, and analog mode. After creating
                        an ADC variable, the program reads the sensor and (after a delay) converts it to a distance in mm 
                        relative to the center of the touch panel.
        '''
        self.PIN_xm.init(mode=Pin.OUT_PP, value=1)  # Initialize pin xm, configure for output and set to low
        self.PIN_xp.init(mode=Pin.OUT_PP, value=0)  # Initialize pin xp, configure for output and set to high
        self.PIN_ym.init(mode=Pin.IN)               # Initialize pin ym, configure for input
        self.PIN_yp.init(mode=Pin.IN)               # Initialize pin yp, configure for input
        self.PIN_ym.init(mode=Pin.ANALOG)           # Initialize pin ym, configure for analog
        
        ## @brief   Analog object for pin ym
        ## @details This will be used to measuring the x distance of a point of contact from the center of the touch panel
        self.ADC_ym = ADC(self.PIN_ym)              # Create an analog object for pin ym
        pyb.udelay(4)                               # Delay 4 microseconds to settle resistor dividers before reading ADC channel

        ## @brief   X-distance of point of contact from center of touch panel
        ## @details A value is read from the touch panel sensors and converted to mm relative to x_c
        self.x_val = (self.ADC_ym.read()-position.x_min)/(position.x_max-position.x_min)*self.w - self.x_c # convert to mm relative to x_c
        return self.x_val                           # Return the calculated value from previous line
        
    def y_coord (self):    
        '''
        @brief          This method reads the vertical potentiometer and outputs a y-distance from center for a contact point
        @details        This method initializes pins and sets them to input/output, high/low, and analog mode. After creating
                        an ADC variable, the program reads the sensor and (after a delay) converts it to a distance in mm 
                        relative to the center of the touch panel.
        '''                        
        self.PIN_ym.init(mode=Pin.OUT_PP, value=0)  # Initialize pin ym, configure for output and set to high
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)  # Initialize pin yp, configure for output and set to low
        self.PIN_xm.init(mode=Pin.IN)               # Initialize pin xm, configure for input
        self.PIN_xp.init(mode=Pin.IN)               # Initialize pin xp, configure for input
        self.PIN_xm.init(mode=Pin.ANALOG)           # Initialize pin xm, configure for analog
        
        ## @brief   Analog object for pin xm
        ## @details This will be used to measuring the y distance of a point of contact from the center of the touch panel
        self.ADC_xm = ADC(self.PIN_xm)              # Create an analog object for pin xm
        pyb.udelay(4)                               # Delay 4 microseconds to settle resistor dividers before reading ADC channel
        
        ## @brief   Y-distance of point of contact from center of touch panel
        ## @details A value is read from the touch panel sensors and converted to mm relative to y_c
        self.y_val = (self.ADC_xm.read()-position.y_min)/(position.y_max-position.y_min)*self.l - self.y_c # convert to mm relative to y_c
        return self.y_val                           # Return the calculated value from previous line
        
    def z_coord (self): 
        '''
        @brief          This method reads the voltage at the center node when both resistor dividers are energized and outputs a boolean representing if panel is being touched
        @details        This method initializes pins and sets them to input/output, high/low, and analog mode. After creating
                        an ADC variable, the program reads the sensor and (after a delay) determines if the panel is being touched
        '''    
        self.PIN_xm.init(mode=Pin.OUT_PP, value=0)  # Initialize pin xm, configure for output and set to low
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)  # Initialize pin yp, configure for output and set to high
        self.PIN_ym.init(mode=Pin.IN)               # Initialize pin ym, configure for input
        self.PIN_xp.init(mode=Pin.IN)               # Initialize pin xp, configure for input
        self.PIN_ym.init(mode=Pin.ANALOG)           # Initialize pin ym, configure for analog
        self.ADC_ym = ADC(self.PIN_ym)              # Create an analog object for pin ym 
        pyb.udelay(4)                               # Delay 4 microseconds to settle resistor dividers before reading ADC channel
    
        ## @brief   z holds the value read from the sensor representing the touch or lack thereof sensed by the touch panel
        ## @details This value is then converted to a binary 1 or 0 based upon its value
        self.z = self.ADC_ym.read()                 # Read an analog value from ADC object
        if self.z <= 4000:                          # Since reading on pin ym, if reading is approx. 4095 then no contact will be sensed
            ## @brief   This value specifies if the touch panel is sensing a touch or not
            ## @details This value variable will be 1 if touch is sensed, else 0
            self.z_val = True                       #     To account for some fluctuation, I compare to 4000 and set z_val to boolean accordingly 
        else:
            self.z_val = False
        return self.z_val
        
    def my_position(self):
        '''
        @brief          This method calls methods x_coord(),y_coord(),and z_coord(),and returns their outputs as a tuple
        @details        The tuple returned by this method contains the x and y distances from the center and a binary 1 or 0
                        based upon if the touch panel is being touched or not.
        '''   
        ## @brief   Variable that contains the previous x_coord
        ## @details Will use this to calculate the change in x coordinate
        self.last_x = self.my_x 
        
        ## @brief   Variable that contains the previous y_coord
        ## @details Will use this to calculate the change in y coordinate
        self.last_y = self.my_y 

        self.my_x = self.x_coord()                       # call x_coord() and store its output as self.my_x
        self.my_y = self.y_coord()                       # call y_coord() and store its output as self.my_y
        
        ## @brief   Variable specifying if the touch panel is sensing a touch (1) or not (0)
        ## @details This value is determined in z_coord()
        self.my_z = self.z_coord()                       # call z_coord() and store its output as self.my_z
        
        ## @brief   Variable that contains the change in x_coord from two previous values
        ## @details Can be used to calculate x component of velocity
        self.delta_x = self.my_x - self.last_x
        
        ## @brief   Variable that contains the change in y_coord from two previous values
        ## @details Can be used to calculate y component of velocity       
        self.delta_y = self.my_y - self.last_y

        ## @brief   Tuple containing my_x, my_y, and my_z
        ## @details These values are calculated in x_coord(), y_coord(), and z_coord()        
        self.my_coords = (self.my_x/1000,self.my_y/1000,self.my_z,self.delta_x/1000,self.delta_y/1000) # create tuple from results of called functions
        
        return self.my_coords                            # return the tuple
        
    
# Test code for testing my_position() and channel timing, including the pin mapping, w, l, x_c, and y_c values I used when testing
if __name__ == "__main__":

    ## @brief   Pin variable representing x-minus
    ## @details This was aribitrarily defined based upon which touch panel and CPU pins
    ##          were found to be connected using a continuity test    
    xm = Pin(Pin.cpu.A6) 
    
        ## @brief   Pin variable representing x-plus
    ## @details This was aribitrarily defined based upon which touch panel and CPU pins
    ##          were found to be connected using a continuity test
    xp = Pin(Pin.cpu.A0) 
    
    ## @brief   Pin variable representing y-minus
    ## @details This was aribitrarily defined based upon which touch panel and CPU pins
    ##          were found to be connected using a continuity test
    ym = Pin(Pin.cpu.A1) 
    
    ## @brief   Pin variable representing y-plus
    ## @details This was aribitrarily defined based upon which touch panel and CPU pins
    ##          were found to be connected using a continuity test
    yp = Pin(Pin.cpu.A7) 
    
    ## @brief   Width of touch panel in units of mm
    ## @details This value was specified manufacturer documentation    
    w = 176   

    ## @brief Length of touch panel in units of mm
    ## @details This value was specified manufacturer documentation              
    l = 99.36  

    ## @brief   X-distance from bottom left edge of touch panel to center, in units of mm
    ## @details We assume the x-distance to the center of the touch panel is half of the width measurement             
    x_c = w/2    

    ## @brief Y-distance from bottom left edge of touch panel to center, in units of mm
    ## @details We assume the y-distance to the center of the touch panel is half of the length measurement          
    y_c = l/2               
    
    ## @brief   Create instance object "tst" for testing
    ## @details This object contains all four pins, the width and length of the touch panel, 
    #           and the coordinates of its center    
    tst = position(xp,xm,yp,ym,w,l,x_c,y_c)  
    
    ## @brief   Take time stamp before reading channels
    ## @details This time stamp will then be used in the calculation of the time necessary to read all channels    
    begin = utime.ticks_us()                 
    
    ## @brief   Call my_position function to return a tuple containing the x,y, and z coordinates.
    ## @details The x and y coordinates represent the distance in mm from point of contact on touch panel 
    #           to the center of the panel and the z coordinate is a boolean specifying if panel is 
    #           being touched or not
    current_position = tst.my_position()     
    
    ## @brief   Take time stamp after reading channels
    ## @details This time stamp will then be used in the calculation of the time necessary to read all channels
    end = utime.ticks_us()                   
    
    ## @brief   Calculate the number of microseconds it took to read all channels
    ## @details This is accomplished by subtracting the end time stamp from the beginning time stamp    
    timing = utime.ticks_diff(end,begin)     
    
    print(current_position)                  # output function output
    print('All three channels were read and conversions were executed in '+ str(timing) + ' microseconds')