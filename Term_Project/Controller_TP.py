'''
@file      Controller_TP.py
@brief     This file contains the controller class used to institute the ball balancing table control system.
@details   This file contains a controller object that acts as a closed loop system. The working principle of the object
           is that it takes in inputs from the touchpad and encoders, determines the duty cycle to apply to each motor, and applies 
           the duty cycle to each motor. Upon construction, a controller object is passed the relevant gain vectors, reference position,
           motor, and encoder for each of the two degrees of freedom, as well as the relevant touchpad object and interval at which the run method
           is to run. The run() method is run at the rate specified by interval in main.py.
@author    Hunter Brooks and Ryan Funchess
@date      March 15, 2021
'''

import pyb
import utime
import math

## @brief   Constant pi value used for conversion purposes.
## @details This value was used in update_rad() and get_delta_rad().
pi = math.pi

class controller:
    '''
    @brief  This Class implements a closed loop control algorithm
    '''
    ## @brief   Flag used to determine when to stop looking for maximum gain values
    ## @details This was used to because unreasonably high values were calculated after the ball left the platform
    Gate = True  
    
    ## @brief   Boolean value that determines if this is the first reading being collected or not
    ## @details The function of this variable is to set the initial velocity of the ball to zero since 
    #           this is our initial condition
    first_reading = True
    
    ## @brief   Maximum x gain due to xdot
    ## @details This was stored for testing purposes
    G1xmax=0
    
    ## @brief   Maximum x gain due to thetadot
    ## @details This was stored for testing purposes
    G2xmax=0
    
    ## @brief   Maximum x gain due to x
    ## @details This was stored for testing purposes
    G3xmax=0
    
    ## @brief   Maximum x gain due to theta
    ## @details This was stored for testing purposes
    G4xmax=0
    
    ## @brief   Maximum y gain due to xdot
    ## @details This was stored for testing purposes
    G1ymax=0
    
    ## @brief   Maximum y gain due to thetadot
    ## @details This was stored for testing purposes
    G2ymax=0
    
    ## @brief   Maximum y gain due to x
    ## @details This was stored for testing purposes
    G3ymax=0
    
    ## @brief   Maximum y gain due to theta
    ## @details This was stored for testing purposes
    G4ymax=0
    
    ## @brief   Gain offset for x motor
    ## @details This value is used if the motor gain is lower than a specified level
    offsetx=20
    
    ## @brief   Gain offset for y motor
    ## @details This value is used if the motor gain is lower than a specified level
    offsety=35
    
    ## @brief   Motor saturation limit
    ## @details This was used to limit the quick, aggressive movements that would throw the ball off the platform
    sat_limit=85
    
    def __init__(self,kx,ky,posref,motx,moty,encx,ency,tscreen,interval):
        '''
        @brief This method constructs a controller object based on passed objects
        @param kx List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the x DOF
        @param ky List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the y DOF
        @param posref List of length 2 containing the x and y reference position for where the ball should rest
        @param motx Motor Driver object that controls the motor for the X-DOF
        @param moty Motor Driver object that controls the motor for the Y-DOF
        @param encx Encoder Driver object that monitors the angle of the motor for the X-DOF
        @param ency Encoder Driver object that monitors the angle of the motor for the Y-DOF
        @param tscreen Touch Screen Driver object that monitors x and y position of ball
        @param interval Integer that holds the desired time between FSM runs in ms
        
        '''        
        
        ## @brief   List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the x DOF
        ## @details This was used when determining the duty for the x motor
        self.kx=kx
        
        ## @brief   List of length 4 containing gains relevant to ball velocity, omega, ball position, and theta respectively for the y DOF
        ## @details This was used when determining the duty for the y motor
        self.ky=ky
        
        ## @brief   List of length 2 containing the x and y reference position for where the ball should rest
        ## @details This was assumed to be the center of the touch panel
        self.posref=posref
        
        ## @brief   Motor Driver object that controls the motor for the X-DOF
        ## @details This object is used to set the duty cycle of the x motor
        self.motx=motx
        
        ## @brief   Motor Driver object that controls the motor for the Y-DOF
        ## @details This object is used to set the duty cycle of the y motor
        self.moty=moty
        
        ## @brief   ncoder Driver object that monitors the angle of the motor for the X-DOF
        ## @details This object is used to calculate the current x encoder angle
        self.encx=encx
        
        ## @brief   Encoder Driver object that monitors the angle of the motor for the Y-DOF
        ## @details This object is used to calculate the current y encoder angle
        self.ency=ency
        
        ## @brief   Touch Screen Driver object that monitors x and y position of ball
        ## @details This object is used to calculate the current ball position and velocity
        self.tscreen=tscreen
        
        ## @brief   Integer that holds the desired time between FSM runs in ms
        ## @details An acceptable value for this is 20
        self.interval=interval
        
        ## @brief   The integer timestamp for the first iteration
        ## @details This is used to calculate when the next when the next calculation should occur
        self.start_time = utime.ticks_ms()
        # print('Start time:'+str(self.start_time))
        
        ## @brief   The integer "timestamp" for when the next run should be
        ## @details The next calculations should occur at a time in ms of start_time + interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## @brief   The largest acceptable encoder angle in radians. If this is exceeded, the motors will be disabled.
        ## @details This is used as a safety precaution so the encoders will not rotate too far and damage the system.
        self.too_large = 40*(2*pi/360)
        
        ## @brief   Length of the lever arm in m
        ## @details This value was given in the lab 6 handout
        self.rm=0.06
        
        ## @brief   Distance from table pivot to table push rod connection in m
        ## @details This value was given in the lab 6 handout
        self.lp=0.11

        
    def run(self):
        '''
        @brief  This method runs the closed loop control system
        @details This method is run in a While(true) loop in main.py. This method
        works cooperatively in that it first checks if it is time to run based on the passed interval, and passes otherwise.
        If the current time is equal to or past the denoted next time to run, the position and velocity of the ball as well as the 
        angle and angular velocity of the table are found in both degrees of freedom. These values are then stored in two vectors that contain
        all relevant information for each degree of freedom. These vectors are then "corrected" by subtracting our observed position
        by our desired position to obtain our error signal. The corrected vectors for each degree of freedom are then
        passed to a control method that computes the duty cycle to apply to the motor based on our numerical MATLAB analysis. These duty cycles
        are finally applied to each motor respectively and the next time for this method to run is specified based on the interval attribute.
        '''
        curr_time=utime.ticks_ms() #current time 
        if utime.ticks_diff(curr_time,self.next_time)>0: #if current time is past the designated next time
            positions = self.scan_positions() #obtain x, xdot, y, ydot
            angles = self.scan_angles()  #obtain theta x, theta y, theta dot x, theta dot y for the table
            x = [positions[1], angles[3], positions[0], angles[1]] #store xdot, theta ydot, x, theta in x, represents x DOF
            y = [positions[3], angles[2], positions[2], angles[0]] #store ydot, theta xdot, y, theta in y, represents y DOF
            
            x = self.correctDOF1(x) #subtract measured values from reference x position to get error signal
            y = self.correctDOF2(y) #subtract measured values from reference y position to get error signal
            Dx = self.controlDOF1(x) #obtain duty cycle to apply to x motor based on MATLAB analysis/tuning
            Dy = self.controlDOF2(y) #obtain duty cycle to apply to y motor based on MATLAB analysis/tuning
            
            self.motx.set_duty(Dx) #Apply output duty cycle to motor x
            self.moty.set_duty(Dy) #Apply output duty cycle to motor y
        
            self.next_time = utime.ticks_add(self.next_time, self.interval) #set the next time for the task to actually run
            
    def scan_positions(self): 
        '''
        @brief Handles ball kinematics (position and velocity of ball x and y directions)
        @return retval Tuple of kinematics values containing (in order): ball x position, ball x velocity
        ball y position, ball y velocity
        '''
        ball_kinematics = self.tscreen.my_position()  # ball_kinematics is vector with length 5 containing [x,y,z,xd,yd]
        if ball_kinematics[2] == False: #If the ball is no longer in contact with the table
        
            ## @brief   Flag used to determine when to stop looking for maximum gain values
            ## @details This was used to because unreasonably high values were calculated after the ball left the platform
            self.Gate = False #stop looking for maximum gain values
            self.motx.disable() #disable the motors
            
        elif ball_kinematics[2] == True: #if the ball is in contact with the table
		        self.motx.enable() #enable the motors

        x_current = ball_kinematics[0] #current x value for ball
        delta_x_current = ball_kinematics[3] #current delta x for ball
        xd_current = delta_x_current/(self.interval/1000) #current ball velocity in x direction
        
        y_current = ball_kinematics[1] #current y value for ball
        delta_y_current = ball_kinematics[4] #current delta y for ball
        yd_current = delta_y_current/(self.interval/1000) #current ball velocity in y direction
        
        if self.first_reading == True: #if this is the first reading being taken
            ## @brief   Boolean value that determines if this is the first reading being collected or not
            ## @details The function of this variable is to set the initial velocity of the ball to zero since 
            #           this is our initial condition
            self.first_reading = False #make sure no other reading is read as the first reading
            xd_current = 0 #initial velocity is zero
            yd_current = 0 #initial velocity is zero
            
        retval=(x_current,xd_current,y_current,yd_current)
        return retval
    
    def scan_angles(self):
        '''
        @brief Handles kinematics of table rotation
        @return retval Tuple containing table angle about x, table angle about y,
        table velocity about x, table velocity about y respectively
        '''
      
        E_angle_x = self.encx.update_rad()   # positive x encoder angle is negative y table angle
        E_angle_y = self.ency.update_rad()    # positive y encoder angle is positive x table angle

        if E_angle_x > self.too_large  or E_angle_x < -1*(self.too_large) or E_angle_y > self.too_large  or E_angle_y < -1*(self.too_large): #if angle of encoder too large
           self.motx.disable()  #disable motor
           
        Table_x_angle = (self.rm/self.lp)*E_angle_y #table x angle from encoders using gear ratio
        Table_y_angle = -(self.rm/self.lp)*E_angle_x #table y angle from encoders using gear ratio

        E_delta_x = self.encx.get_delta_rad()   # Encoder change in angle in x direction
        E_vel_x =  E_delta_x/(self.interval/1000)                 # Encoder velocity in x direction
        E_delta_y = self.ency.get_delta_rad()    # Encoder change in angle in y direction
        E_vel_y =  E_delta_y/(self.interval/1000)                 # Encoder velocity in y direction

        Table_x_vel = (self.rm/self.lp)*E_vel_y #table velocity about x
        Table_y_vel = -(self.rm/self.lp)*E_vel_x #table velocity about y
        retval=(Table_x_angle, Table_y_angle, Table_x_vel, Table_y_vel)
        return retval

    def correctDOF1(self,x):
        '''
        @brief Correct the measurements taken to obtain an error signal for the x DOF
        @details This method subtracts the reference position from measured position in order to 
        obtain an error signal upon which the control voltage can be sourced. Since the reference position for
        the ball is the middle of the table, posref[0] is 0 for this balacning situation.
        @param x List containing xdot, theta dot, x, theta relevant to x direction
        @return x List containing error xdot, theta dot, x, theta relevant to x direction
        '''
        x[2]=x[2]-self.posref[0] #obtain error signal by substracting reference ball position from measured x
        
        return x
    
    def correctDOF2(self,y):
        '''
        @brief Correct the measurements taken to obtain an error signal for the y DOF
        @details This method subtracts the reference position from measured position in order to 
        obtain an error signal upon which the control voltage can be sourced. Since the reference position for
        the ball is the middle of the table, posref[1] is 0 for this balacning situation.
        @param y List containing ydot, theta dot, y, theta relevant to y direction
        @return y List containing error ydot, theta dot, y, theta relevant to y direction
        '''
        y[2]=y[2]-self.posref[1] #obtain error signal by substracting reference ball position from measured y
        return y
    
    def controlDOF1(self,x):
        '''
        @brief Obtain voltage to apply to Motor X based on controls scheme
        @param x List containing error signals for xdot, theta dot, x, theta
        @return D Duty cycle to apply to motor x
        '''
        #Put angles in RADIANS
        G1=x[0]*self.kx[0] #gain due to xdot
        G2=x[1]*self.kx[1] #gain due to omega
        G3=x[2]*self.kx[2] #gain due to x
        G4=x[3]*self.kx[3] #gain due to theta
        if self.Gate == True: #if data collection should still occur
            if(abs(G1)>self.G1xmax): #if current G1 greater than max G1
                ## @brief   Maximum x gain due to xdot
                ## @details This was stored for testing purposes
                self.G1xmax=abs(G1) #set new max value
            if(abs(G2)>self.G2xmax): #if current G2 greater than max G2
                ## @brief   Maximum x gain due to thetadot
                ## @details This was stored for testing purposes
                self.G2xmax=abs(G2) #set new max value
            if(abs(G3)>self.G3xmax): #if current G3 greater than max G3
                ## @brief   Maximum x gain due to x
                ## @details This was stored for testing purposes
                self.G3xmax=abs(G3) #set new max value
            if(abs(G4)>self.G4xmax): #if current G4 greater than max G4 
                ## @brief   Maximum x gain due to theta
                ## @details This was stored for testing purposes
                self.G4xmax=abs(G4) #set new max value
            
        D=G1+G2+G3+G4 #new duty cycle=sum of G
        if D==0: #avoids dividing by zero
            return D     
        elif(abs(D)<self.offsetx): #if requested D too small
            D=self.offsetx*D/abs(D) #set D to minimum required voltage to turn motor in right direction
        else: #high enough motor voltage
            choice=[abs(D),self.sat_limit]
            return (min(choice)*D/abs(D)) #return the minimum value between saturation limit and the requested duty cycle
        
    def controlDOF2(self,y):
        '''
        @brief Obtain voltage to apply to Motor X based on controls scheme
        @param y List containing error signals for xdot, theta dot, x, theta
        @return D Duty cycle to apply to motor x
        '''       
        G1=y[0]*self.ky[0] #gain due to xdot
        G2=y[1]*self.ky[1] #gain due to omega
        G3=y[2]*self.ky[2] #gain due to x
        G4=y[3]*self.ky[3] #gain due to theta
        
        if self.Gate == True: #if data collection should 
            if(abs(G1)>self.G1ymax): #if current G1 greater than max G1
                ## @brief   Maximum y gain due to xdot
                ## @details This was stored for testing purposes
                self.G1ymax=abs(G1) #set new max value
            if(abs(G2)>self.G2ymax):#if current G2 greater than max G2
                ## @brief   Maximum y gain due to thetadot
                ## @details This was stored for testing purposes
                self.G2ymax=abs(G2)#set new max value
            if(abs(G3)>self.G3ymax):#if current G3 greater than max G3
                ## @brief   Maximum y gain due to x
                ## @details This was stored for testing purposes
                self.G3ymax=abs(G3)#set new max value
            if(abs(G4)>self.G4ymax):#if current G4 greater than max G4
                ## @brief   Maximum y gain due to theta
                ## @details This was stored for testing purposes
                self.G4ymax=abs(G4)  #set new max value
       
        D=G1+G2+G3+G4#new duty cycle=sum of G
        if D==0: #avoids dividing by 0
            return D     
        elif(abs(D)<self.offsety): #if requested D too small
            D=self.offsety*D/abs(D) #set D to minimum required voltage to turn in right direction
        else: #high enough motor voltage
            choice=[abs(D),self.sat_limit]
            return (min(choice)*D/abs(D)) #return the minimum value between saturation limit and the requested duty cycle
        