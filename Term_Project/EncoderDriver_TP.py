'''
@file      EncoderDriver_TP.py
@brief     This file contains an encoder driver that continually returns the updated encoder angle.
@details   This driver contains seven methods that operate cohesively to continuously calculate the angle of the encoder.  
           These methods include: (1): __init__() - initializes encoder objects, (2): update() - updates the calculated angular
           position of the encoder, (3): get_position() - returns the most recently updated encoder position, (4): set_position() - 
           resets the position of the encoder to a specified value, (5): get_delta - returns the difference in recorded position 
           between the two most recent position measurements, (6): update_rad(), and (7): get_delta_rad(). This file takes pin and timer objects, and configures the timer objects
           to encoder mode, with a counter that changes when either CH1 or CH2 changes. When either channel changes by one logic level,
           the timer reads one tick of motion. Since there are 4000 ticks per encoder revoluation, we can use this measurement of ticks to 
           calulate the angular position at any given moment. To calculate the current position of the encoder (returned by update()), 
           we work in units of ticks, then convert at this end to angle.  The current tick value is measured from the encoder and the tick
           value from the previous measurement is subtracted from it.  The result of this operation is our raw delta ticks value. 
           The raw delta ticks value is then compared to the acceptable range of ticks delta ticks (-period/2 to period/2, where period = 65535).
           If acceptable, the delta ticks is then converted to units of angles by the fact that there are 4000 ticks per 360 degrees. This value
           is used by the to the previous anglular position value, returned from the get_position() function, in the update() function. 
           This value is then kept in the get_position() function for the next iteration.  This driver can also calculate the encoder angle and 
           delta angle in units of radians by converting from ticks to radians with the conversion: ticks (4000 ticks/(2*pirad) = rad.
@author    Hunter Brooks and Ryan Funchess
@date      March 8, 2021
'''

# Import the necessary module for this driver.
import pyb
# The following two lines of code are used to receive an error message if an error occurs when executing the program.
import micropython
micropython.alloc_emergency_exception_buf(200)  # the buffer contains 200 bytes of memory
import math

## @brief   Constant pi value used for conversion purposes.
## @details This value was used in update_rad() and get_delta_rad().
pi = math.pi

class EncoderDriver:
    ''' 
    @brief   This class implements an encoder driver for the ME405 board which continually returns the updated encoder angle. 
    @details This driver contains five methods that operate cohesively to continuously calculate the angle of the encoder.  
             These methods include: (1): __init__() - initializes encoder objects, (2): update() - updates the calculated angular
             position of the encoder, (3): get_position() - returns the most recently updated encoder position, (4): set_position() - 
             resets the position of the encoder to a specified value, (5): get_delta - returns the difference in recorded position 
             between the two most recent position measurements. This file takes pin and timer objects, and configures the timer objects
             to encoder mode, with a counter that changes when either CH1 or CH2 changes.
    '''
    
    # Class Variable:
    
    ## @brief   this is the magnitude of the timer period and is used to check for timer underflow or overflow
    #  @details this value is used to test if a delta ticks value is acceptable. if not, underflow or 
    #           overflow occured. 
    period = 65535
    
    ## @brief   CPR stands for Cycles per Revolution
    ## @details Since there are 4000 timer ticks per revolution and four timer ticks per cycle,
    #           there are 1000 cycles per revolution
    CPR=1000
    
    def __init__ (self, pin_E_CH1, pin_E_CH2, timer):
         '''
         @brief Creates an encoder driver by initializing pins and timers.
         @param pin_E_CH1      A pyb.Pin object to use as the channel 1 pin.
         @param pin_E_CH2      A pyb.Pin object to use as the channel 2 pin.
         @param timer          A pyb.Pin object to use as the ticks counter.
         '''
         ## @brief   A pyb.Pin object to use as the channel 1 pin.
         ## @details This is specified when creating an encoder object.         
         self.pin_E_CH1      = pin_E_CH1
         
         ## @brief   A pyb.Pin object to use as the channel 2 pin.
         ## @details This is specified when creating an encoder object.         
         self.pin_E_CH2      = pin_E_CH2
         
         ## @brief   A pyb.Pin object to use as the ticks counter.
         ## @details This is specified when creating an encoder object.        
         self.timer          = timer
         
         self.timer.channel(1,pyb.Timer.ENC_AB,pin=self.pin_E_CH1) # configure timer channel 1 to encoder mode, with counter changes when CH1 or CH2 changes 
         self.timer.channel(2,pyb.Timer.ENC_AB,pin=self.pin_E_CH2) # configure timer channel 2 to encoder mode, with counter changes when CH1 or CH2 changes 

         ## @brief   Variable containing current timer tick count
         ## @details This is used to calculate the raw_delta_count         
         self.current_count    = 0    # initialize initial count to 0
         
         ## @brief   Variable containing current angular position in units of ticks
         ## @details This is found by adding the change in angular position by previous angular position
         self.current_position = 0    # initialize initial angular position to zero
         print('Creating an encoder driver')
        
    def update(self):
        '''  
        @brief   Updates and returns the recorded position of the encoder in ticks.
        @details This is done by calling delta_position() function to calculate the
                 most recent change in encoder angle, calling get_position() function to return the previous angular position and adding
                 the two values.
        '''
        ## @brief   Variable containing change in angular position between previous two measurements in units of ticks
        ## @details This value is calculated in get_delta() by calculating the delta ticks
        self.delta_position = self.get_delta()      # calculate change in angular position from previous two encoder readings
        
        ## @brief   Variable containing the last calculated angular position in units of ticks
        ## @details This value is found using get_position() 
        self.last_position  = self.get_position()   # grab position value from previous reading
        self.current_position = self.last_position + self.delta_position  # calculate the current angular position by adding 
        return self.current_position                                      #   the previous position to the most recent change in angular position
        
    def get_position(self):
        ''' 
        @brief   Resets the angular position of the encoder to a specified value.
        @details This can be helpful in zeroing an encoder if necessary.
        '''
        
        return self.current_position
        
    def set_position(self,updated_position):
        ''' 
        @brief   Resets the angular position of the encoder to a specified value in degrees.
        @details This can be helpful in zeroing an encoder if necessary.
        '''
        ## @brief   Variable containing specified current positon by user in units of ticks
        ## @details This can be used to zero the encoder angular position
        self.updated_position = updated_position
        self.current_position = self.updated_position
        return self.current_position
        
    def get_delta (self):
        ''' 
        @brief   Calculates and returns the difference in position between the two most recent position measurements in ticks.
        @details This value can then be used to calculate the encoder angular velocity when divided by collection period.
        ''' 
        ## @brief   Variable containing the previous timer tick count
        ## @details This is then later used to calculate the change in encoder angle in get_delta()        
        self.last_count = self.current_count      # store previous measured tick count in last_count
        self.current_count = self.timer.counter() # measure current tick count and store in current_count

        ## @brief   Variable containing the uncorrected value for change in timer ticks 
        ## @details This is then corrected if neccessary or used if acceptable.        
        self.raw_delta_count = self.current_count - self.last_count  # raw_delta_count = change in ticks from two most recent readings
        
        if self.raw_delta_count >= -self.period/2 and self.raw_delta_count <= self.period/2:  # if delta count is between +/- period/2, its an 
            self.delta_count = self.raw_delta_count                                           #    acceptable delta
            
        elif self.raw_delta_count < -self.period/2:   # Correct for counter overflow by adding period to most recent ticks count
            self.current_count = self.period + self.current_count
            
            ## @brief   Variable containing the corrected value for change in timer ticks 
            ## @details This value will then be converted to delta_position in units of ticks
            self.delta_count = self.current_count - self.last_count
            
        else:                                         # Correct for counter underflow by subtracting period from most recent ticks count
            self.current_count = self.current_count - self.period
            self.delta_count = self.current_count - self.last_count
        
        
        self.delta_position = self.delta_count # convert from ticks to angular position in degrees by multiplying by (360/4000)
        return self.delta_position 
    
    def update_rad(self):
        '''
        @brief Updates and returns the recorded position of the encoder in radians. 
        @details This is done by returning the most recent ticks value from update() and then converting to radians
        '''
        pos_ticks=self.update()
        pos_rad=pos_ticks/(4*self.CPR)*(2*pi) #convert from ticks to rad
        return pos_rad
    
    def get_delta_rad(self):
        ''' 
        @brief   Calculates and returns the difference in position between the two most recent position measurements in units of radians.
        @details This value can then be used to calculate the encoder angular velocity when divided by collection period.
        ''' 
        delta_ticks=self.delta_position
        delta_rad=delta_ticks/(4*self.CPR)*(2*pi) #convert from ticks to rad
        return delta_rad
        
if __name__ =='__main__':
    # The following code represents a test program for the motor class.
    # Any code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the encoder driver

    ## @brief   Encoder 1 channel 1 object
    ## @details This object is attached to pin B6 and set to input    
    pin_E1_CH1 = pyb.Pin(pyb.Pin.cpu.B6,pyb.Pin.IN) 
    
    ## @brief   Encoder 1 channel 2 object
    ## @details This object is attached to pin B7 and set to input
    pin_E1_CH2 = pyb.Pin(pyb.Pin.cpu.B7,pyb.Pin.IN)   
    
    ## @brief   Encoder 2 channel 1 object
    ## @details This object is attached to pin C6 and set to input 
    pin_E2_CH1 = pyb.Pin(pyb.Pin.cpu.C6,pyb.Pin.IN)   
    
    ## @brief   Encoder 2 channel 2 object
    ## @details This object is attached to pin C7 and set to input
    pin_E2_CH2 = pyb.Pin(pyb.Pin.cpu.C7,pyb.Pin.IN)   
    
    # Create the timer objects used for encoder tick counting

    ## @brief   Encoder 1 timer object
    ## @details This object is attached to timer 4 and uses prescaler of 0 and period of 0xFFFF
    timer1 = pyb.Timer(4, prescaler=0, period=0xFFFF)  
    
    ## @brief   Encoder 2 timer object
    ## @details This object is attached to timer 8 and uses prescaler of 0 and period of 0xFFFF  
    timer2 = pyb.Timer(8, prescaler=0, period=0xFFFF)  
    
    # Create a motor object passing in the pins and timer
    
    ## @brief   Encoder object for first encoder
    ## @details This object contains the pin and timer objects for encoder 1
    enc1 = EncoderDriver(pin_E1_CH1, pin_E1_CH2, timer1)
    
    ## @brief   Encoder object for second encoder
    ## @details This object contains the pin and timer objects for encoder 2 
    enc2 = EncoderDriver(pin_E2_CH1, pin_E2_CH2, timer2)
    
    while True:
        print('enc1 [degrees] = ' + str(enc1.update()) + ' enc2 [degrees] = ' + str(enc2.update()))
        pyb.delay(100)