'''
@file      lab0x02.py
@brief     This program computes the aveage user reaction time.
@details   This program calculates the average reaction time it takes a user to 
           press the blue "user button" after the LED is illuminated.  The 
           LED will light up after a delay, which is a random number of miliseconds
           between 2-3 seconds.  The LED will stay illuminated for 1 second, then
           turn back off. While the LED is lit, the user will press the button.
           If the button is pressed multiple times while the LED is lit, the calculated 
           reaction time for that cylce will be computed based upon the last button press.
           When the LED turns off, the process will repeat until the user presses
           CTRL + C.  At this point, the average reaction time will be calcuated and displayed
           to the user.
@author    Hunter Brooks
@date      Jan.28, 2021
'''

# Import any required modules
import pyb
import micropython
import random

# Create emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)  # the buffer contains 200 bytes of memory

## Pin object to use for blinking LED. Attached to PA5
myPin = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Pin object to use for user input in form of the user button press. Attached to PC13
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

## @brief   Timer object to use for measuring timestamps during reaction test.  
## @details The time stamps will be measured when the LED flashes and when the 
#           user button is pressed. This timer variable is the Nucleo's timer 2,
#           with a prescaler of 80 and a period of 7FFFFFFF
myTimer = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)

# Initialization of variables
#  This portion sets each variable to the desired initial value.  

## @brief   This variable represents the random LED delay before it turns on
## @details The LED will turn on after a random nymber of miliseconds between 2-3 seconds
LEDfreq = random.randint(2000,3000)

# this represents the initial state of the LED, off
# this will be toggled between 0 and 1 (off and on) as the LED cycles
myPin.value(0)                      

## @brief   This variable represents the integer number of button presses
#  @details This will be used to calculate the average user response time
count = 0                       

## @brief   This represents the integer total microseconds of reaction time
#  @details This will be used to calculate the average user response time
totalTime = 0                     

## @brief   This represents the integer microseconds of reaction time on the current cycle
#  @details If necessary, this will be part of the summation to compute totalTime
thisTime = 0  

## @brief   This variable represents the time stamp for button press
#  @details This will be used to calculate thisTime
Tmax = 0                            

## @brief   This variable represents the time stamp for the LED turning on
#  @details This will be used to calculate thisTime
Tmin = 0                            

## @brief   This variable represents the error flag
#  @details This will tell the program if the button was pressed on a given cycle
errorFlag = 1                        # flag used to determine if the user button was pressed

## @brief   This variable represents the average user response time
#  @details This will be calculated by summing the reaction times per cycle and then dividing that by the number of cyles
AveResponse = 0                    

# This function represents the callback for the external interrupt connected
#  to the user button.  This takes a timestamp and lowers an error flag

def onButtonPressFCN(IRQ_src):
    '''
    @brief     External interrupt callback, which measures the time stamp and lowers the error flag
    @details   This callback is called whenever the external interrupt is run, which
               is whenever the user button is pressed.  This function measures the 
               final time value that will be used to calculate the change in time between
               when the LED turns on and when the button is pressed, equal to the 
               response time of one cycle. The error flag is also lowered, which 
               notifies the program that the user button was pressed on a particular cycle 
               and that the reaction time should be calculated
    @param     IRQ_src   placeholder variable since callback functions require at least one input
    @return    this function updates variables, but does not display a value
    '''    
    global errorFlag
    global Tmax
    Tmax = myTimer.counter()
    errorFlag = 0

## This is the code that defines the external interrupt object. It is set up to the user button (PC13),
#  triggered on falling edge, no pull up or pull down resistors, callback is defined above

ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

# This is the startup message, which tells the user how to use program
print('Press the user button within one second of the LED turning on!\nWhen finished, press CTRL + C')


#  Main program loop
#  This loop runs continuously until interrupted by a Keyboard Interrupt.  I use
#  a try, except statement that discontinues the program when disrupted by CTRL + C.
#  At this time, the average reaction time is calculated and displayed to the user

while True: 
    try:
        
        pyb.delay(LEDfreq)            # will wait to flash LED for random # miliseconds between 2-3 seconds
        myPin.value(1)                # the LED will turn on
        Tmin = myTimer.counter()      # the time stamp is measured
        pyb.delay(1000)               # will keep LED on for 1 second 
    
        if errorFlag == 0:            # if the user button was pressed, the response time will be calculated,
            thisTime = Tmax - Tmin    #   or else it will remind the user when to press the user button
        
            if thisTime > 0:          # if the response time was positive, then it will be accepted
                totalTime = totalTime + thisTime    # the total response time will be incremented by the cycle reponse time
                count = count + 1     # increment count variable
            
            else:                     # if the response time was negative, then the button was pressed improperly
                print('Please wait to press user button after the LED turns on!')
                       
        else:
            print('Please press user button!')
        
        myPin.value(0)                # the LED will turn off
        LEDfreq = random.randint(2000,3000) # the delay LED initial delay time is reassigned
        errorFlag = 1                 # the error flag is reset for the next pass through the whil loop

    except KeyboardInterrupt:         # if CTRL + C is pressed, then the program performs end procedures
        AveResponse = int(totalTime/count)   # the average response time is calculated and displayed
        print('Your average reaction time was ' + str(AveResponse) + ' microseconds')
        break                         # the program has ended