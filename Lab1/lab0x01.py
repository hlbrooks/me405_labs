'''
@file      lab0x01.py
@brief     This program controls the actions of the vending machine called VendotronˆTMˆ
@details   This program allows the user to input money in the form of a number key
           from "0" -> "7", each key represenging a form of currency (form penny to 
           $20 bill).  The user is also able to purchase a beverage of their choice, 
           listed as follows: "c" = "Cuke", "p" = "Popsi", "s" = "Spryte", and 
           "d" = "Dr.Pupper". The user can also select "e" to eject their money.
           The program can be stopped at any time by pressing "ctrl" + "c"
@author    Hunter Brooks
@date      Jan. 19, 2021
'''

# Begin with code that initializes the program, which is not a part of the FSM

def getChange(price, payment):
    '''
    @brief     computes change from monetary transactions and ejects current balance
    @details   computes change when given an integer value price and integer balance.
               if insufficient funds, the machine returns a message reflecting this 
               and reminds the user the price of the desired beverage and leaves
               the function.  If sufficient funds, the amount of change due is
               calculated and then the least number of coins and bills are calculated.
               Then, the beverage and the user's change are dispensed. The machine
               balance is set to zero and the function is exited.
    @param     price    the price of the desired beverage in cents
    @param     payment  is the variable representing the current balance of the 
                         machine
    '''
    
    # define balance as a global variable
    global balance
    
    # if balance is less than price, an insufficient funds message is displayed,
    # reminding the user how much the desired beverage costs
    if balance < price:  
       
       if cuk == 1:
           print('Insufficient Funds, Cuke = 100 Cents')
       elif pop == 1:
           print('Insufficient Funds, Popsi = 120 Cents')
       elif spr == 1:
           print('Insufficient Funds, Spryte = 85 Cents')
       elif pup == 1:
           print('Insufficient Funds, Dr.Pupper = 110 Cents')
   
    # if sufficient funds calculate the money to return to user
    else: 
       remaining = balance - price
           
       # next, the following lines of code divide the remaining change by each 
       # form of currency in cents, then subtracts it from what is left, in 
       # decending order. This process finds the least number of coins and 
       # bills to return to user
       
       twenties = int(remaining/2000)
       
       remaining = remaining - twenties * 2000
       
       tens = int(remaining/1000) 
       
       remaining = remaining - tens * 1000
       
       fives = int(remaining/500)
       
       remaining = remaining - fives * 500
       
       ones = int(remaining/100)
       
       remaining = remaining - ones * 100
       
       quarters = int(remaining/25)
       
       remaining = remaining - quarters * 25
       
       dimes = int(remaining/10)
       
       remaining = remaining - dimes * 10
       
       nickles = int(remaining/5)
       
       pennies = remaining - nickles * 5
       
       ## @brief   balance represents the current amount of money in units of cents entered by the user
       ## @details this variable is used to determine if sufficient funds were enterred into the machine
       balance = 0
       
       ## @brief   change is a tuple of length eight that represents the coins and bills to be dispensed to the user
       ## @details the order of the coins and bills is in ascending order from (left to right) pennies to
       #           twenty dollar bills
       change = (pennies, nickles, dimes, quarters, ones, fives, tens, twenties)
       
       # the above line of code stores the number of each denomination in
       # its respective location of the change to be dispensed
       
       if cuk == 1:
           print('Vending Cuke, change due is ' + str(change))
       elif pop == 1:
           print('Vending Popsi, change due is ' + str(change))
       elif spr == 1:
           print('Vending Spryte, change due is ' + str(change))
       elif pup == 1: 
           print('Vending Dr.Pupper change due is ' + str(change))
       else:
           print('Eject, change due is ' + str(change))
       # lastly the above line of code displays the output change to the user    
       
       ## @brief   this variable keeps track of what state the program is in during operation
       state = 1

def printWelcome():
    ''' 
    @brief     Prints a VendotronˆTMˆ welcome message with beverage prices
    @details   The welcome message is printed during the initialization state
               and after change is returned when the eject button is pressed
    '''
    print('Welcome to VendotronˆTMˆ, Pricing is as Follows (Values Appear in Cents): \
          \nCuke = 100, Popsi = 120, Spryte = 85, Dr.Pupper = 110')
    pass

import keyboard

## @brief variable that stores the previous key pressed by the user
pushed_key = None

def on_keypress (thing):
    '''
    @brief     Callback which runs when the user presses a key.
    @details   This function interprets a user key press and then acts upon it. If 
               a form of currency is entered, the user is notified of the type of 
               currency and the current balance. If "c", "p", "s" or "d" are
               pressed, a drink was selected, the state variable changes to 2 and
               the proper steps are taken on the next loop through the main program.
               If "e" is pressed, the eject button was pressed, the state variable
               changes to 3 and the proper steps are taken on the next loop
               through the main program. 
    @param     thing   the key pressed by the user which is being handled in this function
    '''
    global pushed_key

    pushed_key = thing.name

keyboard.on_press (on_keypress)  ########## Set callback

# Run a simple loop which responds to some keys and ignores others.
# If someone presses control-C, exit this program cleanly

## @brief   this variable keeps track of what state the program is in during operation
## @details this is used in the FSM
state = 0

while True:   # this is the beginning of the perpetrual loop that will continue
              # until "ctrl" + "c" is pressed

    if state == 0:
        """perform state 0 operations
        this init state, initializes the FSM itself, with all the
        code features already set up and sets each variable to the desired initial
        value
        """
        ## @brief   the price of Cuke
        ## @details this is used getChange() to determine change to be dispensed
        Cuke = 100           
        
        ## @brief the price of Popsi
        ## @details this is used getChange() to determine change to be dispensed
        Popsi = 120          
        
        ## @brief the price of Spryte
        ## @details this is used getChange() to determine change to be dispensed
        Spryte = 85         
        
        ## @brief the price of DrPupper
        ## @details this is used getChange() to determine change to be dispensed
        DrPupper = 110       
        
        ## @brief the Cuke flag which is raised when the "c" key is pressed
        ## @details this flag determines the correct value for "price" before performing 
        ##          the getChange() function
        cuk = 0              
        
        ## @brief the Popsi flag which is raised when the "p" key is pressed
        ## @details this flag determines the correct value for "price" before performing 
        ##          the getChange() function
        pop = 0              
        
        ## @brief the Spryte flag which is raised when the "s" key is pressed
        ## @details this flag determines the correct value for "price" before performing 
        ##          the getChange() function
        spr = 0             
        
        ## @brief the DrPupper flag which is raised when the "d" key is pressed
        ## @details this flag determines the correct value for "price" before performing 
        ##          the getChange() function
        pup = 0              

        ## @brief variable that stores the current machine monetary input
        ## @details this variable is used to calculate the integer value of change to dispense
        balance = 0          
        
        ## @brief variable referenced in getChange() function representing amount of user payment
        ## @details this variable is used to calculate the integer value of change to dispense
        payment = 0
        
        ## @brief variable that holds the value of a beverage in getChange()
        ## @details this variable is used to calculate the integer value of change to dispense
        price = 0            
        
        ## @brief variable that holds the intermediate value when deciding change
        ## @details this integer value of change left to dispense is updated after
        #           determining the integer number of each coin or bill
        remaining = 0        
        
        ## @brief variable representing number of $20 bills to return
        ## @details this integer value is stored in the "change" tuple when dispensing change
        twenties = 0         
        
        ## @brief variable representing number of $10 bills to return
        ## @details this integer value is stored in the "change" tuple when dispensing change
        tens = 0             
        
        ## @brief variable representing number of $5 bills to return
        ## @details this integer value is stored in the "change" tuple when dispensing change
        fives = 0            
        
        ## @brief variable representing number of $1 bills to return
        ## @details this integer value is stored in the "change" tuple when dispensing change
        ones = 0             
        
        ## @brief variable representing number of quarters to return
        ## @details this integer value is stored in the "change" tuple when dispensing change
        quarters = 0         
        
        ## @brief variable representing number of dimes to return
        ## @details this integer value is stored in the "change" tuple when dispensing change
        dimes = 0            
        
        ## @brief variable representing number of nickles to return
        ## @details this integer value is stored in the "change" tuple when dispensing change
        nickles = 0         
        
        ## @brief variable representing number of pennies to return
        ## @details this integer value is stored in the "change" tuple when dispensing change 
        pennies = 0          
        
        ## @brief   change is a tuple of length eight that represents the coins and bills to be dispensed to the user
        ## @details the order of the coins and bills is in ascending order from (left to right) pennies to
        #           twenty dollar bills
        change = 0           # integer value of change to be returned to user
        
        printWelcome()       # calls function that prints startup message
        
        state = 1            # on the next iteration, the FSM will run state 1
        
    elif state == 1:
        """perform state 1 operations
           State 1 analyzes a keystroke and increases the current balance,
           raises flags, changes the state variable, or tells the user to select
           a valid key
        """
  
        try:
        # If a key has been pressed, check if it's a key we care about
            if pushed_key:
                if pushed_key == "0":   # penny
                    balance = balance + 1     # raise balance by a cent
                    print ('Penny, your current balance is ' + str(balance) + ' Cents')
                
                elif pushed_key == "1": # nickle
                    balance = balance + 5     # raise balance by 5 cents
                    print ('Nickle, your current balance is ' + str(balance) + ' Cents')
                
                elif pushed_key == "2": # dime
                    balance = balance + 10    # raise balance by 1o cents
                    print ('Dime, your current balance is ' + str(balance) + ' Cents')
                
                elif pushed_key == "3": # quarter
                    balance = balance + 25    # raise balance by 25 cents
                    print ('Quarter, your current balance is ' + str(balance) + ' Cents')
                
                elif pushed_key == "4": # $1 bill  
                    balance = balance + 100   # raise balance by 100 cents
                    print ('$1 Bill, your current balance is ' + str(balance) + ' Cents')
                
                elif pushed_key =='5': # $5 bill
                    balance = balance + 500   # raise balance by 500 cents
                    print ('$5 Bill, your current balance is ' + str(balance) + ' Cents')
                
                elif pushed_key =='6': # $10 bill
                    balance = balance + 1000  # raise balance by 1000 cents
                    print ('$10 Bill, your current balance is ' + str(balance) + ' Cents')
                
                elif pushed_key =='7': # $20 bill
                    balance = balance + 2000  # raise balance by 2000 cent
                    print ('$20 Bill, your current balance is ' + str(balance) + ' Cents') 
                
                elif pushed_key == 'e': # eject key
                    state = 3           # on the next iteration, the FSM will run state 3

                elif pushed_key =='c':  # Cuke 
                    cuk = 1             # raise Cuke flag
                    state = 2           # on the next iteration, the FSM will run state 2

                elif pushed_key =='p':  # Popsi
                    pop = 1             # raise Popsi flag
                    state = 2           # on the next iteration, the FSM will run state 2

                elif pushed_key =='s':  # Spryte
                    spr = 1             # raise Spryte flag
                    state = 2           # on the next iteration, the FSM will run state 2

                elif pushed_key =='d':  # Dr.Pupper
                    pup = 1             # raise Dr.Pupper flag
                    state = 2           # on the next iteration, the FSM will run state 2
                    
                else: # invalid input
                    print('Invalid Input')                    

                pushed_key = None

        # If Control-C is pressed, this is sensed separately from the keyboard
        # module; it generates an exception, and we break out of the loop
        except KeyboardInterrupt:
            break
    
    elif state == 2:
        """perform state 2 operations
           State 2 handles the vending of beverages and returning proper change
           or notifying the user that the balance was insufficient and reminding 
           them how much the desired beverage costs
        """
        if cuk == 1:                   # Cuke
            price = 100                # used to test if sufficient funds
            getChange(price, payment)  # branch to accounting/vending program
            cuk = 0                    # lower Cuke flag
            
        elif pop == 1:                 # Popsi
            price = 120                # used to test if sufficient funds
            getChange(price, payment)  # branch to accounting/vending program 
            pop = 0                    # lower Cuke flag
            
        elif spr == 1:                 # Spryte
            price = 85                 # used to test if sufficient funds
            getChange(price, payment)  # branch to accounting/vending program
            spr = 0                    # lower Cuke flag
            
        else:                          # Dr.Pupper
            price = 110                # used to test if sufficient funds
            getChange(price, payment)  # branch to accounting/vending program
            pup = 0                    # lower Cuke flag
        
        state = 1                      # on the next iteration, the FSM will run state 1    

    elif state == 3:
        """perform state 3 operations
           State 3 handles the eject button, therefore returning the current 
           balance to the user and reprinting the startup message
        """
        price = 0                      # since no beverage is bought, there are no costs
        getChange(price, payment)      # calculate coins and bills to return
        balance = 0                    # money was returned, so no money in machine
        state = 1                      # on the next iteration, the FSM will run state 1       
        printWelcome()                 # print the welcome message

print ("Control-C has been pressed, so it's time to exit.")
    #keyboard.unhook_all ()