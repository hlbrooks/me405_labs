'''
@file      lab0x03.py
@brief     This program examines the voltage response to a button press using the ADC
@details   This program utilizes a user interface that facilitattes communication between
           the human user and their laptop.  This program consists of two separate 
           scripts: (1) 'lap0x03.py' acting as the front end (what the user sees) and 
           (2) 'main.py' acting as the back end (invisible to the user). The program
           begins by prompting the user to supply an input into the console window.  
           The program then takes this input and tests to see if it is either an uppercase
           or lowercase 'G'.  If it is one of these, the program returns a message with the ASCII
           equivalent and then prompts the user to quickly press the user
           button.  If the user presses the button within around 5 seconds, the back 
           end will measure the voltage response of the user button and transform this into
           a plot of the voltage vs time.  Last, the program creates a .csv file 
           with this data and stores it in the folder in which 'lab0x03.py'
           is located.
@author    Hunter Brooks
@date      Feb. 2, 2021
'''

# Import any required modules
import csv
from matplotlib import pyplot as pyp
import serial
import time

## The above line sets up the serial port, the baud rate, and the timeout flag
ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)

## @brief   This variable represents the index of the voltage being manipulated
#  @details This variable is used to normalize the voltage readings to a scale with
#           a 3.3V max
i = 0

## @brief   This variable acts as a flag that fascilitates the filtering of user inputed characters
#  @details This flag raises once a upper or lowercase 'G' is entered by the user. At this
#           time, the front end will communicate with the backend
Gpressed = 0

## @brief   This variable stores the ASCII value of the user entered character(s)
#  @details This variable is then compared to the ASCII equivalent of uppercase or lowercase
#           'G'.  If true, communication with the backend begins
keyValue = 0

## @brief   This list stores the time data that is split from ADC data after being received from backend
#  @details The indexes of this list are then transformed into integers and appended to list named
#           timeValues which is plotted on the x-axis of voltage vs time plot. The indices
#           are stripped at the leading and trailing square brackets and split at the separating commas
myTime = []

## @brief   This list stores the ADC data that is split from time after being received from backend
#  @details The indexes of this list are then converted to voltage values as floats and appended to
#           list named voltageValues which is plotted on the y-axis of voltage vs time plot. The indices
#           are stripped at the leading and trailing square brackets and split at the separating commas
myVoltage = []

## @brief   This list stores the integer time values that are plotted on the x axis
#  @details Please see myTime variable for more information
timeValues = []

## @brief   This list stores the floated voltage values that are plotted on the y axis
#  @details Please see myVoltage variable for more information
voltageValues = []

def sendChar():
    '''
    @brief     Interprets user input on the front end and sends data to backend
    @details   This function prompts the user to enter a 'G' and accepts both upper
               and lowercase 'G's but rejects all else. The user must input only one
               character for it to be accepted. Once the user has satisfied the 
               prompt, the front end sends the ASCII equivalent to the backend, which
               sends a message in return to the front end that is printed (notifying 
               the user that the correct key was entered). The function is then ended
    @return    This function returns a message from the backend that is printed
    '''        
    global Gpressed                                    # define global variable
    while Gpressed == 0:                               # while G hasn't been pressed, ask for G
    
        ## @brief Variable containing the user's keyboard inputs
        ## @details If an uppercase of lowercase G is enterred, the front end communicates with the
        #           back end, which returns a message to the front end
        inv = input('Please enter a G: ')              # prompt the user to input character
        if len(inv) == 1:                              # the user input single character
        
            ## @brief Variable containing the ASCII equivalent of the user's keyboard input
            ## @details This variable tests for an uppercase of lowercase G
            keyValue = ord(inv)                        # testing for appropriate ASCII equivalent of input
            if keyValue == 71 or keyValue == 103:      # if the proper input, raise Gpressed flag
                Gpressed = 1
                ser.write(str(inv).encode('ascii'))    # write() method writes a specified text to the file
                myval = ser.readline().decode('ascii') # readline() method returns the specified line
                return myval                           # return myval from backend, which gets printed
            else:
                pass
        else:
            pass

def receiveBack():
    '''
    @brief     This function manipulates, plots, and saves data as .csv file
    @details   This file reads the the data sent from the backend of the program
               after the ADC data is collected and the corresponding time matrix
               is created. The time vector is then split from the ADC data, each
               is then stripped of their leading and trailing square brackets, and 
               split at the separating commas.  The ADC data is then converted to
               floated normalized voltage data with max of 3.3V. The time is 
               converted to integers. The voltage vector is plotted vs the time 
               vector, with time in microseconds on the x-axis and volts involtage on the 
               y-axis.  The user is prompted that this process is complete and 
               then a .csv file is created with colums of: numbered index, time, and voltage
               NOTE: A delay of 5 seconds is run before data manipulation occurs to give 
               the user time to press the user button and allow the backend to process the data
    @return    This function returns a plot and .csv file of data
    '''            
    global i                                                       # define global i variable
    time.sleep(5)
                                                  # sleep for 5 seconds
    ## @brief String containing data collected from backend
    ## @details This data will be manipulated in order to plot
    myLine = ser.readline().decode('ascii')                        # receive data from backend
    
    ## @brief List containing data collected from backend
    ## @brief This list separates the time from ADC data
    myList = myLine.split('];[')                                   # split time from ADC
    
    ## @brief List containing time data after stripping the brackets and splitting terms about the commas
    ## @brief This list data will be plotted along with voltage
    myTime = myList[0].strip('[]').split(',')                      # strip and split time data
    
    ## @brief List containing ADC data after stripping the brackets and splitting terms about the commas
    ## @brief This list data will be plotted along with time
    myVoltage = myList[1].strip('[]').split(',')                   # strip and split ADC data
    
    while i < len(myVoltage):                                      # append data as long as there 
        voltageValues.append(float(myVoltage[i])*3.3/4090)         #  is data to append
        timeValues.append(int(float(myTime[i])))
        i = i + 1
    pyp.plot(timeValues,voltageValues)          # plot the time on the x-axis and voltage on y-axis
    pyp.xlabel("Time [Microseconds]")           # label x-axis with time in microseconds           
    pyp.ylabel("Voltage [Volts]")               # label y-axis with voltage in volts
    print('The Voltage vs Time Plot is Complete!')  # notify user that plot is complete
    
    with open('StepResponse.csv', 'w', newline='') as csvfile:    # this process creates .csv file
        fieldnames = ['Number','Time','Voltage']
        thewriter = csv.DictWriter(csvfile, fieldnames=fieldnames)
        thewriter.writeheader()
        Number = -1
        i = -1
        while Number < len(myVoltage) - 1:
            Number += 1
            i += 1
            thewriter.writerow({'Number':Number,'Time':timeValues[i], 'Voltage':voltageValues[i]})
      
            
# Main Code      
for n in range(1):            
    print(sendChar())  # call sendChar() function
    receiveBack()      # call receiveBack() function
    ser.close()        # close the serial port