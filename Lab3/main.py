'''
@file      main.py 
@brief     This file collects data from the ADC when prompted and relays it to front end
@details   This script acts as the back end to 'lab0x03.py' This file begins by waiting to receive
           communication from the front end. Upon receiving this communication, it relays a message
           back to the front end and then awaits the user button to be pressed. Upon being pressed, 
           ADC values are collected at a specified rate, desirable values are appended from the mass 
           buffer that stores all data collected, and a corresponding time value is created and stored
           in a vector that will be processed and plotted to display the time response. The time and 
           voltage values are then sent back to the fornt side to be processed.
@author    Hunter Brooks
@date      Feb. 2, 2021
'''
# Import any required modules
import pyb
import micropython
import array

# creates emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## @brief   This timer will be used to control the frequency of data collection
## @details Timer channel 2 is used at 20000 Hz
myTimer = pyb.Timer(2, freq=20000) 

## @brief   This pin corresponds to our user button
## @details C13 is used and set to input
pinC13 = pyb.Pin (pyb.Pin.board.PC13, mode=pyb.Pin.IN)

## @brief   This analog to dital converter is used to plot the time response of the user button press
## @details I selected Pin A0 as my pin to measure ADC values
myADC = pyb.ADC(pyb.Pin.board.A0) 

## @brief   This buffer stores the voltage and time data from the button press
## @details The array can be described as unsigned short, pyton integer type, 2 minimum bytes
buffy = array.array('H', (0 for index in range(5000))) # unsigned short, pyton integer type, 2 minimum bytes

## @brief   This variable acts as a flag that notifies when the user button has been pressed
#  @details This flag raises once the user button and fascilitates the transitoin to ADC
#           data collection
buttonFlag = 0

## @brief   This variable will be used to tell when 'G' has been successfully entered
#  @details This flag raises once the front end has communicated with the backend
GFlag = 0

## @brief   This vector stores the desirable ADC values to be later manipulated
#  @details This vector appends values stored within buffy that are greater than
#           5 and less than 4090
voltage = [0]

## @brief   This variable keeps track of the current time to pair with indexed ADC value
#  @details This time value is then stored at the index in the time vector that pairs
#           with its stored ADC value
currentTime = 0

## @brief   This vector stores the time values that correspond to the desirable ADC values
#  @details The indices within this vector will then be converted to integers on the front
#           end and will eventually be plotted on the x-axis of the step response plot
time = [0]

## @brief   This variable represents the index of the ADC value stored within buffy
#  @details This variable is used to append the desirable ADC values from buffy, which
#           will be further manipulated, converted to voltage, and plotted vs time
n = 0

# Callback function to run when user button is pressed
def onButtonPressFCN(IRQ_src):
    '''
    @brief     External interrupt callback, which sets button flag
    @details   This callback is called whenever the external interrupt is run, which
               is whenever the user button is pressed.  The purpose of this function
               is to prompt the collection of data using myADC.read_timed function
    @param     IRQ_src  placeholder variable since callback functions require at least one input
    '''    
    global buttonFlag
    buttonFlag = 1

## This is the code that defines the external interrupt object. It is set up to the user button (PC13),
#  triggered on falling edge, no pull up or pull down resistors, callback is defined above

ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

# Import UART from pyb modules
from pyb import UART

## Variable which will be used to write response to front end and facilitate the software handshake
myuart = UART(2)

# Main Code

while GFlag == 0:                 # while statement is looping through until communication is received from front end
    if myuart.any() != 0:         # returns the number of bytes waiting
        ## Variable containing the ASCII equivalent of the letter enterred by the user
        #  Will be printed in a message returned to user on the front end
        val = myuart.readchar()   # returns a single character and returns it as integer
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo. Quick, Press the User Button! ')  # write the buffer of bytes to the bus
        GFlag = 1                 # if 'G' has been sent, set GFlag, which will wait for button to be pressed
    
while buttonFlag == 0:            # this will wait for the user button being pressed
    pass

while buttonFlag == 1:              # this while loop is devoted to handling the response to the user button being pressed
    myADC.read_timed(buffy,myTimer) # when the user button is pressed, store ADC values at frequency specified by myTimer
    while n < 4999:                 # Since the buffer has length of 50000, this while loop tests each index within vector
        if 5 < buffy[n] < 4090:        # this line only stores the ADC values from buffy > 5 and < 4090
            voltage.append(buffy[n])   # append the acceptable values to ADC vector called voltage
            currentTime = currentTime + 1e6/20000  # create time index that corresponds to voltage index value within voltage
            time.append(currentTime)               # vector, incrementing at 50 microsecond intervals since the timer is collecting
            n = n + 1                              #     data every 50 microseconds
        else:                       # if data is not > 5 or < 4090, then disregard it and move to next index
            n = n + 1
    buttonFlag = 0                  # when all indices have been iterated through, the while loop will be exited and the 
                                    #    stored data will be written back to the front end
myuart.write('{:};{:}'.format(time,voltage))   # write the contents of time and voltage to the front end, time then voltage
                                               #   each vector separated by a semicolon
while True:                        # this is where the code will stay after data manipulation functions have completed.
    pass                           #   I have done this to eliminate a string message that was generated at the end of the data
                                   #   sent to the front end when the program on the back end ran to completion, which gave data
                                   #   manipulation issues on the front end